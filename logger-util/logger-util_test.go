package logger_util

import (
	"github.com/labstack/gommon/log"
	"testing"
)

func TestLoggerUtil_GetLogLvl_Debug(t *testing.T) {
	logLvl := GetLogLvl(LogDebug)
	if logLvl != log.DEBUG {
		t.Errorf("Not Match (%s is wrong value)", LogDebug)
	}
}

func TestLoggerUtil_GetLogLvl_Info(t *testing.T) {
	logLvl := GetLogLvl(LogInfo)
	if logLvl != log.INFO {
		t.Errorf("Not Match (%s is wrong value)", LogInfo)
	}
}

func TestLoggerUtil_GetLogLvl_Warn(t *testing.T) {
	logLvl := GetLogLvl(LogWarn)
	if logLvl != log.WARN {
		t.Errorf("Not Match (%s is wrong value)", LogWarn)
	}
}

func TestLoggerUtil_GetLogLvl_Error(t *testing.T) {
	logLvl := GetLogLvl(LogError)
	if logLvl != log.ERROR {
		t.Errorf("Not Match (%s is wrong value)", LogError)
	}
}

func TestLoggerUtil_GetLogLvl_Off(t *testing.T) {
	logLvl := GetLogLvl(LogOff)
	if logLvl != log.OFF {
		t.Errorf("Not Match (%s is wrong value)", LogOff)
	}
}

func TestLoggerUtil_GetLogLvl(t *testing.T) {
	logLvl := GetLogLvl("")
	if logLvl != log.Level() {
		t.Errorf("Not Match (logLvl is not log.Level())")
	}
}
