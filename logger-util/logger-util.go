package logger_util

import "github.com/labstack/gommon/log"

const (
	LogDebug string = "DEBUG"
	LogInfo  string = "INFO"
	LogWarn  string = "WARN"
	LogError string = "ERROR"
	LogOff   string = "OFF"
)

func GetLogLvl(lvl string) log.Lvl {
	switch lvl {
	case LogDebug:
		return log.DEBUG
	case LogInfo:
		return log.INFO
	case LogWarn:
		return log.WARN
	case LogError:
		return log.ERROR
	case LogOff:
		return log.OFF
	}
	return log.Level()
}
