package validator_util

import (
	"testing"
)

func TestValidatorUtil_NewValidatorUtil(t *testing.T) {
	validator := NewValidatorUtil()
	if validator.validator == nil {
		t.Errorf("Not Match (%v is wrong instance)", validator.validator)
	}
}

type TestString struct {
	Test string `validate:"required,script-tag"`
}

func TestValidatorUtil_Validate(t *testing.T) {
	validator := NewValidatorUtil()
	if err := validator.Validate(TestString{Test: "test"}); err != nil {
		t.Errorf("Validation failed (%s)", err.Error())
	}
}
