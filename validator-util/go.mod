module gitlab.com/threetopia/echo-util/validator-util

go 1.21

toolchain go1.22.3

require github.com/go-playground/validator/v10 v10.4.1

require (
	github.com/go-playground/locales v0.13.0 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	golang.org/x/crypto v0.22.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
)
