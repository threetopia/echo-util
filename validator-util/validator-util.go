package validator_util

import (
	"regexp"

	"github.com/go-playground/validator/v10"
)

type ValidatorUtil struct {
	validator *validator.Validate
}

func NewValidatorUtil() *ValidatorUtil {
	validate := validator.New()
	// Register the custom validation function
	validate.RegisterValidation("script-tag", scriptTag)
	return &ValidatorUtil{validator: validate}
}

func (vu *ValidatorUtil) Validate(i interface{}) error {
	if err := vu.validator.Struct(i); err != nil {
		// Optionally, you could return the error to give each route more control over the status code
		return err
	}
	return nil
}

func scriptTag(fl validator.FieldLevel) bool {
	// Get the value of the field
	value := fl.Field().String()
	// Regular expression to match "<script>" or "&lt;script&gt;"
	return !regexp.MustCompile(`(?i)<script>|&lt;script&gt;`).MatchString(value)
}
